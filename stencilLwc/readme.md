## Stencil

![Stencil](/Assets/Images/stencil.png)

## Usage:

**js**
```js
    //stencil
    @track cols = [1,2];
    @track opacs = ['opacity: 1', 'opacity: 0.9', 'opacity: 0.8', 'opacity: 0.7', 'opacity: 0.6', 'opacity: 0.5', 'opacity: 0.4', 'opacity: 0.3', 'opacity: 0.2', 'opacity: 0.1'];
    @track double = true;
```

**html**
```html
<c-stencil-lwc
  double="{double}"
  columns="{cols}"
  opacities="{opacs}"
></c-stencil-lwc>
```
