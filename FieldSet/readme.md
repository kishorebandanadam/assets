## Field Set

@Params:

@param: fieldSetName - Field set api name from setup
@param: sObjectName - object api name

Return all fields existing in that fieldset.